const express = require('express');
const mongoose = require('mongoose');

const productRoute = require('./routes/productRoute')

const app = express();
const port = 3002;

app.use(express.json());
app.use(express.urlencoded({extended:true}))

mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.2q2jw.mongodb.net/REST-API-mini-activity?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

app.use('/products', productRoute)

app.listen(port, ()=>console.log(`Listening to port ${port}`));