const Product = require('../models/product')

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


module.exports.createProduct = (requestBody) => {
	let newProduct = new Product({
		name: requestBody.name,
		price: requestBody.price
	})
	return newProduct.save().then((product, error) => {
		if(error){
			console.log(error)
		}else{
			return product;
		}
	})
}


module.exports.getProduct = (productId) => {
	//The "findById" Mongoose method will look for a task with the same ID provided from the URL
	return Product.findById(productId).then((result, error)=>{
		if(error){
			console.log(error)
			return false;
		//Find successful returns the task object back to the client/Postman
		}else{
			return result;
		}
	})
}


module.exports.deleteProduct = (productId) => {
	return Product.findByIdAndRemove(productId).then((removeProduct, err)=>{
		if(err){
			console.log(err)
			return false;
		}else{
			return removeProduct;
		}
	})
}