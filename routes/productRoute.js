const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController.js')

//Route for getting all products
router.get('/', (req, res)=>{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

//Route for add new product
router.post('/', (req, res)=>{
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))
})

//Route for getting a specific id
router.get('/:id', (req, res)=>{
	productController.getProduct(req.params.id).then(resultFromController => res.send(resultFromController))
})

//Route for delete new product
router.delete('/delete/:id', (req, res)=>{
	productController.deleteProduct(req.params.id).then(resultFromController => res.send(resultFromController))
})



module.exports = router;